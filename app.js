var engines = require("consolidate");
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MemcachedStore = require('connect-memcached')(session);
var flash = require('connect-flash');

// 加载conn到global中
require('./tools/mongoose_conn');

// 加载各models模块
require('./models/course');
require('./models/menu');
require('./models/user');

var routers = require('./controller/centre');

var Setting = require('./config/setting');
var Config = Setting.prime;
var Config_r = Setting.memcacheq;

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', engines.ejs);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: Config_r.secret,
    key: Config_r.key,
    proxy: 'true',
    cookie: {
        domain:''
    },
    store: new MemcachedStore({
        hosts: Config_r.host+":"+Config_r.port,
        prefix: 'webservice-'
    })
}));
app.use(flash());

app.use('/', routers);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.end(err.toString());
});

app.listen(Config.PORT, function () {
    console.log('Server is running on ' + Config.PORT + ' ...');
});