var mongoose = require('mongoose');
var config_m = require('../config/setting').mongo;

// 将数据库连接对象挂载到全局对象中
global.db = mongoose.connect(config_m.conn_url);