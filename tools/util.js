/**
 * 空对象检测
 */
var isEmptyObject = function (obj) {
    for (var o in obj) {
        return false;
    }
    return true;
}

/**
 * 非空对象检测
 */
var isNotEmptyObject = function (obj) {
    return !isEmptyObject(obj);
}

/**
 * 空字符串检测
 */
var isEmptyString = function (str) {
    if (str === null || str === undefined || str === '') {
        return true;
    }
    return false;
}

/**
 * 非空字符串检测
 */
var isNotEmptyString = function (str) {
    return !isEmptyString(str);
}

/**
 * 日期格式化
 */
var dateFormat = function (fmt) {
    var o = {
        'M+': this.getMonth() + 1,                 //月份
        'd+': this.getDate(),                    //日
        'h+': this.getHours(),                   //小时
        'm+': this.getMinutes(),                 //分
        's+': this.getSeconds(),                 //秒
        'q+': Math.floor((this.getMonth() + 3) / 3), //季度
        'S': this.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
        }
    }
    return fmt;
}

module.exports = {
    isEmptyObject: isEmptyObject,
    isNotEmptyObject: isNotEmptyObject,
    isEmptyString: isEmptyString,
    isNotEmptyString: isNotEmptyString,
    dateFormat: dateFormat
}