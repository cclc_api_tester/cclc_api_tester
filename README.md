# README #

API及数据测试工具API接口说明

注：如果无特殊说明，result类型均以json形式返回

1. API数据拉取接口
url: /tester/api/V1/getNodejsApi (其中V1为版本号，V2，V3以此类推)
method: get
params: 无
result: postman二次处理之后的json数据
role: 拉取所有的API数据

2. mysql验证接口
url: /tester/api/V1/testSql
method: post
params: statement
result: true/false/error
role: 对指定mysql语句进行验证

3. mongodb验证接口
url: /tester/api/V1/testMongo
method: post
params: statement
result: true/false/error
role: 对指定mongo语句进行验证

4. API执行接口
url: /tester/api/V1/executeApi
method: post
params: 类似如下示例的json数据
    ```
    {
        apiurl: 'c1_login',
        method: 'POST',
        form: {
            mobile: '13484275307',
            pwd: '111111'
        }
    }
    ```
result: 相应json串
role: 通过传参的形式调用相应API结果，并返回相应的数据结果

5. 测试场景数据拉取接口
url: /tester/api/V1/getSceneApi
method: get
params: 无
result: 测试场景的json数据
role: 向前端提供场景的json数据以便其进行展示

6. 测试流程拉取接口
url: /tester/api/V1/getTestProcess
method: get
params: procId
result: 测试流程json串
role: 向前端提供流程展示项

7. 用户注册模块
url: /tester/api/V1/userRegister
method: post
params: name, password
result: true/false/error
role: 用户注册

8. 用户登录模块
url: /tester/api/V1/userLogin
method: post
params: name, password
result: userId/error
role: 用户登录






















