var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var async = require('async');

var Util = require('../tools/util');

var courseSchema = new Schema({
    // 子项ID号
    menuId: { required: true, type: String },
    // 数据项数组
    tabList: { required: true, type: Object },
    // 任务ID号序列数组
    stepIndex: { required: true, type: Array },
    // 用户ID号
    userId: { type: String },
    // 响应结果
    response: { type: String }
});

courseSchema.statics = {
    /**
     * 保存操作项
     */
    saveCourse: function (row, cb) {
        async.waterfall([
            function (next) {
                course.create(row, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 移除操作项
     */
    removeCourseById: function (row, cb) {
        async.waterfall([
            function (next) {
                course.remove({ _id: row.id }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    removeCourseByMenuId: function (row, cb) {
        async.waterfall([
            function (next) {
                course.remove({ menuId: row.menuId }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 修改操作项
     */
    updateCourseById: function (row, cb) {
        async.waterfall([
            function (next) {
                course.update(
                    { _id: row.id },
                    { $set: row },
                    function (err, result) {
                        next(err, result);
                    });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    updateCourseByMenuId: function (row, cb) {
        async.waterfall([
            function (next) {
                course.update(
                    { menuId: row.menuId },
                    { $set: row },
                    function (err, result) {
                        next(err, result);
                    });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取操作项
     */
    getOne: function (cb) {
        async.waterfall([
            function (next) {
                course.findOne(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取所有操作项
     */
    getAll: function (cb) {
        async.waterfall([
            function (next) {
                course.find(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 根据ID获取操作项
     */
    getOneById: function (row, cb) {
        async.waterfall([
            function (next) {
                course.findOne({ _id: row.id }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 根据menuId获取操作项
     */
    getOneByMenuId: function (row, cb) {
        async.waterfall([
            function (next) {
                course.findOne({ menuId: row.menuId }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    }
};

// 注册model
var course = global.db.model('course', courseSchema);
module.exports = course;