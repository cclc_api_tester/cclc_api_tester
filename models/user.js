var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var async = require('async');

var Util = require('../tools/util');

var userSchema = new Schema({
    // 用户名
    name: { required: true, type: String },
    // 密码
    password: { required: true, type: String },
    // 权限
    power: { type: Number, default: 1}
});

userSchema.statics = {
    /**
     * 保存用户
     */
    saveUser: function (row, cb) {
        async.waterfall([
            function (next) {
                user.findOne(row, function (err, result) {
                    if (!err && Util.isNotEmptyObject(result)) {
                        next(err);
                    } else {
                        next('user is excited');
                    }
                });
            },
            function (next) {
                user.create(row, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 移除用户
     */
    removeUserById: function (row, cb) {
        async.waterfall([
            function (next) {
                user.remove({ _id: row.userId }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 修改用户
     */
    updateUserById: function (row, update, cb) {
        async.waterfall([
            function (next) {
                user.update(
                    { _id: row.userId },
                    { $set: update },
                    function (err, result) {
                        next(err, result);
                    });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取用户
     */
    getOne: function (cb) {
        async.waterfall([
            function (next) {
                user.findOne(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取所有用户
     */
    getAll: function (cb) {
        async.waterfall([
            function (next) {
                user.find(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 根据ID获取用户
     */
    getOneById: function (row, cb) {
        async.waterfall([
            function (next) {
                user.findOne({ _id: row.userId }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 根据ID获取用户
     */
    getOneByParam: function (row, cb) {
        async.waterfall([
            function (next) {
                user.findOne({ name: row.name, password: row.password }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    }
};

// 注册model
var user = global.db.model('user', userSchema);
module.exports = user;