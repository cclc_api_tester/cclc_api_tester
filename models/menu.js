var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var async = require('async');

var Course = global.db.model('course');
var Util = require('../tools/util');

var menuSchema = new Schema({
    // 文件夹/子项名
    text: { required: true, type: String },
    // 父节点Id
    parentId: { required: true, type: String },
    // 图标
    iconCls: { type: String },
    // 文件夹/文件区分标志
    status: { requried: true, type: Number },
    // 备注
    des: { type: String }
});

menuSchema.statics = {
    /**
     * 保存节点
     */
    saveMenu: function (row, cb) {
        async.waterfall([
            function (next) {
                menu.create(row, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 移除节点
     */
    removeMenuById: function (row, cb) {
        async.waterfall([
            function (next) {
                menu.findOne({ parentId: row._id }, function (err, result) {
                    if (!err && Util.isNotEmptyObject(result)) {
                        next('could not has children!');
                    } else {
                        next(null);
                    }
                });
            },
            function (next) {
                menu.remove({ _id: row._id }, function (err, result) {
                    next(null);
                });
            },
            function (next) {
                Course.removeCourseByMenuId({ menuId: row._id }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 修改节点
     */
    updateMenuById: function (row, cb) {
        async.waterfall([
            function (next) {
                menu.update(
                    { _id: row._id },
                    { $set: row },
                    function (err, result) {
                        next(err, result);
                    });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取节点
     */
    getOne: function (cb) {
        async.waterfall([
            function (next) {
                menu.findOne(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 获取所有节点
     */
    getAll: function (cb) {
        async.waterfall([
            function (next) {
                menu.find(function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    },
    /**
     * 根据ID获取节点
     */
    getOneById: function (row, cb) {
        async.waterfall([
            function (next) {
                menu.findOne({ _id: row._id }, function (err, result) {
                    next(err, result);
                });
            }
        ], function (err, result) {
            return cb(err, result);
        });
    }
};

// 注册model
var menu = global.db.model('menu', menuSchema);
module.exports = menu;