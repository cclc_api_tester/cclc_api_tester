//生成api树
function genApiTree() {
    $("#api").tree({
        lines : true,
        url : genApiUrl,
        onDblClick : function(node){
            if(!node.children) {
                var tab = getContentTab();
                var index = $("#tabs").tabs('getTabIndex', tab);
                if(index === 0) {
                    $.messager.alert('系统提示', "请选择一个Demo文件!");
                } else {
                    var id = tab[0].id;
                    $("#" + id).append(genApiItem(node));
                    $("#" + id).scrollTop($("#" + id).height());
                }
            }
        }
    });
}

//生成Api模块
function genApiItem(node) {
    var id = genId();
    var content = '<a id="' + id + '"><div class="item"><div class="item-left"><div class="item-top">'
        + '<span class="title" name="title">' + node.text.substring(0, 12) + '</span><span class="tip1" onclick="removeItem(this)">&#10006</span>'
        + '<span class="tip2" onclick="hideText(this)">▼</span><span class="id" name="step">' + id + '</span>'
        + '<input type="hidden" name="api_id" value="' + node.id + '"/><input type="hidden" name="api_method" value="' + node.attributes.method + '"/>'
        + '<input type="hidden" name="api_url" value="' + node.attributes.url + '"/>'
        + '</div><div class="item-center"><div><label>params:</label><textarea rows="4" name="params">{\n';
    if(node.attributes.data === null) {
        
    } else if(node.attributes.data.length > 0) {
        for(var i = 0; i < node.attributes.data.length - 1; i ++) {
            content += '\t"' + node.attributes.data[i].key + '":'+ '"",\n';
        }
        content += '\t"' + node.attributes.data[node.attributes.data.length - 1].key + '":'+ '""\n';
    }
    content += '}</textarea><span onclick="insert(this)">+</span></div>'
        + '<div class="hide-text"><label>assert:</label><textarea rows="4" name="assert"></textarea><span onclick="insert(this)">+</span></div>'
        + '<div class="hide-text"><label>mysql:</label><textarea rows="4" name="mysql"></textarea><span onclick="insert(this)">+</span></div>'
        + '<div class="hide-text"><label>mongo:</label><textarea rows="4" name="mongo"></textarea></div>'
        + '</div></div><div class="item-right" name="record"></div></div></a>';
    return content;
}

//隐藏显示输入
function hideText(t) {
    $(t).parent().next().toggle();
    $(t).parent().parent().next().toggle();
}

//自动编号
function genId() {
    return "s" + Math.round(new Date().getTime()/1000);
}

//移除一个Api模块样式
function removeItem(t) {
    $(t).parent().parent().parent().remove();
}

//添加一个text
function insert(t) {
    var type = $(t).html();
    if(type === "+") {
        $(t).parent().next().show();
        $(t).html("-");
    } else {
        $(t).parent().next().hide();
        $(t).html("+");
    }
}
