'use strict';

const ipAddr = [
    '',
    'http://127.0.0.1:3000',
    'http://192.168.31.203:3000'
];

const url = ipAddr[0];

const genApiUrl = url + "/tester/api/V1/getNodejsApi";
const genDemoUrl = url + "/tester/api/V1/getMenu";
const saveMenuUrl = url + "/tester/api/V1/saveMenu";
const deleteMenuUrl = url + "/tester/api/V1/deleteMenu";
const sendDbUrl = url + "/tester/api/V1/testStatement";
const saveDemoUrl = url + "/tester/api/V1/saveCourse";
const findDemoUrl = url + "/tester/api/V1/getCourseByMenuId";
const runDemoUrl = url + "/tester/api/V1/batchNodeApi";
const editParentIdUrl = url + "/tester/api/V1/modifyParentIdById";

$(function() {

    //生成demo树
    genDemoTree();

    //生成API树
    genApiTree();
});

//获取当前Tab
function getContentTab() {
    var tab = $("#tabs").tabs("getSelected");
    return tab;
}