//生成工具栏
function genTabsTool() {
    var toolbar = '<div style="width:100%; height:30px; background: #F4F4F4; position: fixed; border: 1px solid #95B8E7;">'
        + '<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveDemo()">保存</a>'
        + '<a href="#" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="runDemo()">执行</a></div>';
    return toolbar;
}

//打开一个Tab
function openTabs(node) {
    if($("#tabs").tabs('exists', node.text)) {
        $("#tabs").tabs('select', node.text);
    } else {
        var menuId = node.id;
        $.post(findDemoUrl, {menuId: menuId}, function(data) {
            if(data.status === 0) {
                $("#tabs").tabs('add', {
                    id:node.id,
                    title:node.text,
                    content:genTabsTool(),
                    closable:true
                });
            } else {
                $("#tabs").tabs('add',{
                    id:node.id,
                    title:node.text,
                    content:genTabsTool(),
                    closable:true
                });
                var tab = getContentTab();
                var id = tab[0].id;
                $("#" + id).append(genDemoContent(data.data.tabList, data.data.stepIndex));
            }
        });
    }
}

//保存Demo 包括api顺序 参数
function saveDemo() {
    var tabList = {};
    var stepIndex = [];
    var tab = getContentTab();
    var menuId = tab[0].id;
    var content = $("#" + menuId);
    var titles = content.find("span[name=title]");
    var methods = content.find("input[name=api_method]");
    var steps = content.find("span[name=step]");
    var ids = content.find("input[name=api_id]");
    var urls = content.find("input[name=api_url]");
    var params = content.find("textarea[name=params]");
    var asserts = content.find("textarea[name=assert]");
    var mysqls = content.find("textarea[name=mysql]");
    var mongos = content.find("textarea[name=mongo]");

    for (var i = 0; i < steps.length; i++) {
        tabList[steps[i].innerHTML] = {};
        tabList[steps[i].innerHTML].id = ids[i].value;
        tabList[steps[i].innerHTML].method = methods[i].value;
        tabList[steps[i].innerHTML].apiurl = urls[i].value;
        tabList[steps[i].innerHTML].title = titles[i].innerHTML;
        tabList[steps[i].innerHTML].assert = asserts[i].value;
        tabList[steps[i].innerHTML].mysql = mysqls[i].value;
        tabList[steps[i].innerHTML].mongo = mongos[i].value;
        tabList[steps[i].innerHTML].form = params[i].value;
        stepIndex.push(steps[i].innerHTML);
    }

    $.post(saveDemoUrl, {tabList: JSON.stringify(tabList), menuId: menuId, stepIndex: JSON.stringify(stepIndex)}, function (data) {
        if(data.status === 1) {
            $.messager.alert('系统提示', "保存成功！");
        } else {
            $.messager.alert('系统提示', '保存失败！');
        }
    });
}

//执行Demo
function runDemo() {
    var tab = getContentTab();
    var menuId = tab[0].id;
    var flag = 1;
    $.post(runDemoUrl, {menuId: menuId}, function(data) {
        console.log(data);
        if(data.status === 2) {
            $.messager.alert('系统提示', "没有可执行的内容，请保存文件或添加内容！");
        } else if(data.status === 1) {
            for(var i = 0; i < data.stepIndex.length; i ++) {
                var index = data.stepIndex[i];
                if(data.records[index].assert.length > 0) {
                    for(var j = 0; j < data.records[index].assert.length; j ++) {
                        if(data.records[index].assert[j].error) {
                            genRecord(menuId, data, index, data.records[index].assert[j].error);
                            $.messager.alert('系统提示', "出错了！<br><font color='red'>" + data.records[index].assert[j].assert
                            + "&nbsp;&nbsp;>>&nbsp;&nbsp;" + data.records[index].assert[j].result + "</font>");
                             window.location.href = "#" + index;
                             flag = 0;
                            break;
                        }
                    }
                }
            }
            if(flag === 1) {
                genRecord(menuId, data, data.id, "success");
                $.messager.alert('系统提示', "执行成功！");
            }
        } else if(data.status === 0) {
            genRecord(menuId, data, data.id, data.error);
            $.messager.alert('系统提示', "出错了！<br><font color='red'>error:" + data.error + "</font>");
            window.location.href = "#" + data.id;
        } else {
            $.messager.alert('系统提示', "未知错误！");
        }
    });
}

//生成Demo内容
function genDemoContent(tabList, stepIndex) {
    var content = '';
    for(var i = 0; i < stepIndex.length; i ++) {
        var index = stepIndex[i];

        content += '<a id="' + index + '"><div class="item"><div class="item-left"><div class="item-top">'
            + '<span class="title" name="title">' + tabList[index].title.substring(0, 12) + '</span><span class="tip1" onclick="removeItem(this)">&#10006</span>'
            + '<span class="tip2" onclick="hideText(this)">▼</span><span class="id" name="step">' + index + '</span>'
            + '<input type="hidden" name="api_id" value="' + tabList[index].id + '"/><input type="hidden" name="api_method" value="' + tabList[index].method + '"/>'
            + '<input type="hidden" name="api_url" value="' + tabList[index].apiurl + '"/></div>'
            + '<div class="item-center"><div><label>params:</label><textarea rows="4" name="params">' + tabList[index].form + '</textarea><span onclick="insert(this)">+</span></div>'
            + '<div class="' + (tabList[index].assert === "" ? "hide-text" : "") + '"><label>assert:</label><textarea rows="4" name="assert">' + tabList[index].assert + '</textarea><span onclick="insert(this)">+</span></div>'
            + '<div class="' + (tabList[index].mysql === "" ? "hide-text" : "") + '"><label>mysql:</label><textarea rows="4" name="mysql">' + tabList[index].mysql + '</textarea><span onclick="insert(this)">+</span></div>'
            + '<div class="' + (tabList[index].mongo === "" ? "hide-text" : "") + '"><label>mongo:</label><textarea rows="4" name="mongo">' + tabList[index].mongo + '</textarea></div></div>'
            + '</div><div class="item-right" name="record"></div></div></a>';
    }

    return content;
}

//生成日志
function genRecord(menuId, data, errorIndex, msg) {
    var content = $("#" + menuId);
    var records = content.find("div[name=record]");
    var left_items = content.find("div[class=item-left]");
    var items = content.find(".item-top");
    for(var i = 0; i < data.stepIndex.length; i ++) {
        var index = data.stepIndex[i];
        records[i].style.height = left_items[i].offsetHeight + 'px';
        if(errorIndex === null) {
            records[i].innerHTML = '运行结果:<br><br>' + msg + '<br><br>';
            items[i].style.background = "cadetblue";
        } else {
            if(index === errorIndex) {
                items[i].style.background = "red";
                records[i].innerHTML = '运行结果:<br><br><font color="red">' + msg + '</font><br><br>';
                records[i].innerHTML += '请求参数:<br><br>' + JSON.stringify(data.records[index].nodeApi.request.form, "", "<br>") + '<br><br>';
                records[i].innerHTML += '返回数据:<br><br>' + JSON.stringify(data.records[index].nodeApi.result, "", "<br>") + '<br><br>';
                records[i].innerHTML += '断言:<br><br>';
                for(var m = 0; m < data.records[index].assert.length; m ++) {
                    records[i].innerHTML += data.records[index].assert[m].assert + "&nbsp;&nbsp;>>&nbsp;&nbsp;" + data.records[index].assert[m].result + "<br>";
                }
                records[i].innerHTML += "<br><br>";
                records[i].innerHTML += '数据库:<br><br>';
                for(var j = 0; j < data.records[index].db.length; j ++) {
                    records[i].innerHTML += data.records[index].db[j].str + "&nbsp;&nbsp;>>&nbsp;&nbsp;" + (data.records[index].db[j].result.status ? "1" : "0") + "<br>";
                }
                records[i].innerHTML += "<br><br>";
                break;
            } else {
                records[i].innerHTML = '运行结果:<br><br>success<br><br>';
                items[i].style.background = "cadetblue";
            }
        }
        records[i].innerHTML += '请求参数:<br><br>' + JSON.stringify(data.records[index].nodeApi.request.form, "", "<br>") + '<br><br>';
        records[i].innerHTML += '返回数据:<br><br>' + JSON.stringify(data.records[index].nodeApi.result, "", "<br>") + '<br><br>';
        records[i].innerHTML += '断言:<br><br>';
        for(var m = 0; m <data.records[index].assert.length; m ++) {
            records[i].innerHTML += data.records[index].assert[m].assert + "&nbsp;&nbsp;>>&nbsp;&nbsp;" + data.records[index].assert[m].result + "<br>";
        }
        records[i].innerHTML += "<br><br>";
        records[i].innerHTML += '数据库:<br><br>';
        for(var j = 0; j < data.records[index].db.length; j ++) {
            records[i].innerHTML += data.records[index].db[j].str + "&nbsp;&nbsp;>>&nbsp;&nbsp;" + (data.records[index].db[j].result.status ? "1" : "0") + "<br>";
        }
        records[i].innerHTML += "<br><br>";
    }
}
