//生成demo树
function genDemoTree() {
    $("#demo").tree({
        dnd : true,
        lines : true,
        url : genDemoUrl,
        onDblClick : function(node) {
            if(node.attributes.status === 2) {
                openTabs(node);
            }
        },
        onDrop: function(targetNode, source, point){
	        var parentNode = $("#demo").tree('getNode', targetNode);
            if(parentNode.attributes.status === 0) {
                genDemoTree();
                return false;
            } else if(parentNode.attributes.status === 2) {
                genDemoTree();
                return false;
            } else if(parentNode.attributes.status === 1) {
                $.post(editParentIdUrl, {menuId:source.id, parentId: parentNode.id}, function(data) {
                    genDemoTree();
                });
            }
        }
    });
}

//欲添加demo
function demoPreadd() {
    var node = $("#demo").tree('getSelected');
    if(node !== null && node.attributes.status === 0) {
        //打开文件夹对话框
        openAddDlg("添加文件夹", node);
        $("#status").val(1);
    } else if (node !== null && node.attributes.status === 1) {
        //打开demo对话框
        openAddDlg("添加Demo", node);
        $("#status").val(2);
    } else if(node === null || node.attributes.status === 2) {
        $.messager.alert('系统提示', "请选择一个目录！");
    }
}

//欲修改demo
function demoPreedit() {
    var node = $("#demo").tree('getSelected');
    if(node === null || node.attributes.status === 0){
        $.messager.alert('系统提示', "请选择一个文件或者文件夹！");
    }else if(node !== null && node.attributes.status === 1) {
        //打开文件夹对话框
        openEditDlg("修改文件夹", node);
    } else if(node !== null && node.attributes.status ===2) {
        //打开Demo对话框
        openEditDlg("修改Demo", node);
    }
}

//打开添加对话框
function openAddDlg(title, node) {
    resetDlg();
    $("#dlg").dialog("open").dialog("setTitle", title);
    $("#parentId").val(node.id);
}

//打开修改对话框
function openEditDlg(title, node) {
    resetDlg();
    $("#dlg").dialog("open").dialog("setTitle", title);
    $("#id").val(node.id);
    $("#name").val(node.text);
    $("#des").val(node.attributes.des);
}

//关闭对话框
function closeDlg() {
    $("#dlg").dialog("close");
    resetDlg();
}

//保存demo
function save() {
    var parentId = $("#parentId").val();
    var text = $("#name").val();
    var id = $("#id").val();
    var des = $("#des").val();
    var status = $("#status").val();
    $.post(saveMenuUrl, {parentId: parentId, text: text, id: id, des: des, status: status}, function (data) {
        if(data.status === 1) {
            $.messager.alert("系统提示", "保存成功！");
            genDemoTree();
        } else {
            $.messager.alert("系统提示", "保存失败！");
        }
        closeDlg();
    });
}

//重置对话框
function resetDlg() {
    $("#name").val("");
    $("#parentId").val("");
    $("#id").val("");
    $("#des").val("");
    $("#status").val("");
}

//删除
function demoDelete() {
    var node = $("#demo").tree('getSelected');
    if(node === null || node.attributes.status === 0){
        $.messager.alert('系统提示', "请选择一个文件或者文件夹！");
    }else if(node !== null && node.attributes.status === 1) {
        //是否删除文件夹
        $.messager.confirm('系统提示','确定要删除这个文件夹吗？',function(r){
            if (r){
                $.post(deleteMenuUrl, {id: node.id}, function(data) {
                    if(data.status === 1) {
                        $.messager.alert("系统提示", "删除成功！");
                        genDemoTree();
                    } else if(data.status === 2) {
                        $.messager.alert("系统提示", "该文件夹下存在Demo文件，不能删除！");
                    } else {
                        $.messager.alert("系统提示", "删除失败！");
                    }
                });
            }
        });
    } else if(node !== null && node.attributes.status ===2) {
        //是否删除Demo
        $.messager.confirm('系统提示','确定要删除这个Demo文件吗？',function(r){
            if (r){
                $.post(deleteMenuUrl, {id: node.id}, function (data) {
                    if(data.status === 1) {
                        $.messager.alert("系统提示", "删除成功！");
                        genDemoTree();
                    } else {
                        $.messager.alert("系统提示", "删除失败！");
                    }
                });
            }
        });
    }
}