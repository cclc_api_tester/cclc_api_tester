var router = require('express').Router();

var Util = require('../tools/util');

router.get('/', function (req, res) {
    res.render('index');
});

module.exports = router;