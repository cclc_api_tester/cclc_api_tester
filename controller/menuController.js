var router = require('express').Router();

var Util = require('../tools/util');

var menuService = require('../service/menuService');
var getMenu = menuService.getMenu;
var addMenu = menuService.addMenu;
var deleteMenu = menuService.deleteMenu;
var modifyMenu = menuService.modifyMenu;

/**
 * name: 测试场景数据拉取接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/getMenu', function (req, res) {
    getMenu(null, function (err, data) {
        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    });
});

/**
 * name: 添加/修改节点接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/saveMenu', function (req, res) {
    var id = req.body.id;
    var text = req.body.text;
    var parentId = req.body.parentId;
    var des = req.body.des;
    var status = req.body.status;

    if (Util.isEmptyString(id)) {
        addMenu({
            text: text,
            parentId: parentId,
            des: des,
            status: status
        }, function (err, data) {
            if (err) {
                res.jsonp(err);
                return;
            }
            res.jsonp(data);
        });
    } else {
        modifyMenu({
            _id: id,
            text: text,
            des: des
        }, function (err, data) {
            if (err) {
                res.jsonp(err);
                return;
            }
            res.jsonp(data);
        });
    }
});

/**
 * name: 测试场景移除接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/deleteMenu', function (req, res) {
    var id = req.body.id;

    deleteMenu({
        _id: id
    }, function (err, data) {
        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    });
});

/**
 * 重新设置parentId
 */
router.post('/tester/api/V1/modifyParentIdById', function (req, res) {
    var menuId = req.body.menuId;
    var parentId = req.body.parentId;

    modifyMenu({
        _id: menuId,
        parentId: parentId
    }, function (err, data) {
        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    }); 

});

module.exports = router;