var router = require('express').Router();

var userService = require('../service/userService');
var userRegister = userService.userRegister;
var userLogin = userService.userLogin;

var Util = require('../tools/util');

/**
 * name: 注册模块
 * version: V1
 * return: true/false/error
 */
router.post('/tester/api/V1/userRegister', function (req, res) {
    var name = req.body.name;
    var password = req.body.password;

    if (Util.isEmptyString(name) || Util.isEmptyString(password)) {
        res.jsonp('name or password is empty!');
        return;
    }

    userRegister({
        name: name,
        password: password
    }, function (err, data, store) {

        // 存入session对象中
        if (Util.isNotEmptyObject(store)) {
            for (var o in store) {
                req.session[o] = store[o];
            }
        }

        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    });
});

/**
 * name: 登录模块
 * version: V1
 * return: userId/error
 */
router.post('/tester/api/V1/userLogin', function (req, res) {
    var name = req.body.name;
    var password = req.body.password;

    if (Util.isEmptyString(name) || Util.isEmptyString(password)) {
        res.jsonp({
            error: 'name or password is empty!'
        });
        return;
    }

    userLogin(
        {
            name: name,
            password: password
        }, function (err, data, store) {

            // 存入session对象中
            if (Util.isNotEmptyObject(store)) {
                for (var o in store) {
                    req.session[o] = store[o];
                }
            }

            if (err) {
                res.jsonp(err);
                return;
            }
            res.jsonp(data);
        });

});

module.exports = router;