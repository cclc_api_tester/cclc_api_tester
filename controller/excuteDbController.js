var router = require('express').Router();

var excuteDbService = require('../service/excuteDbService');
var testSql = excuteDbService.testSql;
var testMongo = excuteDbService.testMongo;

var Util = require('../tools/util');

/**
 * name: mysql验证接口
 * version: V1
 * return: true/false/error
 */
router.post('/tester/api/V1/testSql', function (req, res) {
    var statement = req.param('statement');

    testSql({
        statement: statement
    }, function (err, data) {
        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    });
});

/**
 * name: mongodb验证接口
 * version: V1
 * return: true/false/error
 */
router.post('/tester/api/V1/testMongo', function (req, res) {
    var statement = req.param('statement');

    testMongo({
        statement: statement
    }, function (err, data) {
        if (err) {
            res.jsonp(err);
            return;
        }
        res.jsonp(data);
    });
});

/**
 * 语句验证接口
 */
router.post('/tester/api/V1/testStatement', function (req, res) {
    var statement = req.param('str');
    var type = req.param('type');

    if (type == 0) {
        testSql({
            statement: statement
        }, function (err, results) {
            if (err) {
                res.jsonp(err);
                return;
            }

            if (Util.isEmptyString(results.data) || results.data.length === 0) {
                results.status = 2;
            }

            res.jsonp(results);
        });
    } else if (type == 1) {
        testMongo({
            statement: statement
        }, function (err, results) {
            if (err) {
                res.jsonp(err);
                return;
            }

            if (Util.isEmptyString(results.data) || results.data.length === 0) {
                results.status = 2;
            }
            
            res.jsonp(results);
        });
    }

});

module.exports = router;