var router = require('express').Router();

var pageController = require('./pageController');
var nodeApiController = require('./nodeApiController');
var userController = require('./userController');
var excuteDbController = require('./excuteDbController');
var menuController = require('./menuController');
var courseController = require('./courseController');

router.use('/', pageController);
router.use('/', nodeApiController);
router.use('/', userController);
router.use('/', excuteDbController);
router.use('/', menuController);
router.use('/', courseController);

module.exports = router;