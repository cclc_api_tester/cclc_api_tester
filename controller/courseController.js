var router = require('express').Router();

var Course = global.db.model('course');
var Util = require('../tools/util');

var courseService = require('../service/courseService');
var getCourseByMenuId = courseService.getCourseByMenuId;
var addCourse = courseService.addCourse;
var modifyCourse = courseService.modifyCourse;

/**
 * name: 流程数据拉取接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/getCourseByMenuId', function (req, res) {
    var menuId = req.body.menuId;

    getCourseByMenuId({ menuId: menuId }, function (err, data) {
        if (err) {
            res.jsonp({
                status: 0,
                error: err
            });
            return;
        }

        if (Util.isEmptyObject(data)) {
            res.jsonp({
                status: 0,
                error: 'data is not exist'
            });
            return;
        }

        res.jsonp({
            status: 1,
            data: data
        });
    });
});

/**
 * name: 添加/修改流程接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/saveCourse', function (req, res) {
    var tabList = req.body.tabList;
    var menuId = req.body.menuId;
    var stepIndex = req.body.stepIndex;

    stepIndex = eval('(' + stepIndex + ')');
    tabList = eval('(' + tabList + ')');

    Course.getOneByMenuId({ menuId: menuId }, function (err, data) {

        // 如果存在流程则直接进行修改操作，反之则进行添加操作
        if (!err && Util.isNotEmptyObject(data)) {
            modifyCourse({
                menuId: menuId,
                tabList: tabList,
                stepIndex: stepIndex
            }, function (err, data) {
                if (err) {
                    res.jsonp({
                        error: err
                    });
                    return;
                }
                res.jsonp(data);
            });
        } else {
            addCourse({
                menuId: menuId,
                tabList: tabList,
                stepIndex: stepIndex
            }, function (err, data) {
                if (err) {
                    res.jsonp({
                        error: err
                    });
                    return;
                }
                res.jsonp(data);
            });
        }
    });
});



module.exports = router;