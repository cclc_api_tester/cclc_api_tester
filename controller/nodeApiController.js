﻿var router = require('express').Router();

var Config_apiBox = require('../config/setting').apiBox;

var nodeApiService = require('../service/nodeApiService');
var getNodeApi = nodeApiService.getNodeApi;
var batchNodeApi = nodeApiService.batchNodeApi;

var courseService = require('../service/courseService');
var getCourseByMenuId = courseService.getCourseByMenuId;
var orderTaskByMenuId = courseService.orderTaskByMenuId;

var Util = require('../tools/util');

/**
 * name: API数据拉取接口
 * version: V1
 * return: json数据
 */
router.post('/tester/api/V1/getNodejsApi', function (req, res) {
    var resJson = [];

    for(var i = 0; i < Config_apiBox.length; i++) {
        var fileInfo = Config_apiBox[i];
        var filePath = fileInfo.path;
        var jsonData = getNodeApi(filePath);

        resJson.push(jsonData);
    }

    res.jsonp(resJson);
});

/**
 * name: API执行接口
 * version: V1
 * return: 执行结果，json串格式待定
 */
router.post('/tester/api/V1/batchNodeApi', function (req, res) {

    var menuId = req.body.menuId;

    orderTaskByMenuId({ menuId: menuId }, function (err, tasks_obj) {
        if(err) {
            res.jsonp(err);
        } else {
            batchNodeApi(tasks_obj, function (err, data) {
                res.jsonp(data);
            });
        }
    });

});

module.exports = router;