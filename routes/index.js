var express = require('express');

var router = express.Router();

var getNodejsApi = require('../modules/get_node_api');
var testSql = require('../modules/test_sql');
var testMongo = require('../modules/test_mongo');
var executeApi = require('../modules/execute_api');


var fileName = './public/other/NodejsApi.json.postman_collection';

var result = result ? result : getNodejsApi(fileName);

/**
 * name: API数据拉取接口
 * version: V1
 * return: 带根部的json数据
 */
router.get('/tester/api/V1/getNodejsApi', function (req, res) {
    res.json(result);
});
/**
 * name: API数据拉取接口
 * version: V2
 * return: 不带根部的json数据
 */
router.get('/tester/api/V2/getNodejsApi', function (req, res) {
    res.json(result.folders);
});
/**
 * name: API数据拉取接口
 * version: V3
 * return: 不带根部的jsonp数据
 */
router.get('/tester/api/V3/getNodejsApi', function (req, res) {
    res.jsonp(result.folders);
});

/**
 * name: mysql验证接口
 * version: V1
 * return: true/false/error
 */
router.post('/tester/api/V1/testSql', function (req, res) {
    var statement = req.param('statement');

    testSql(statement, function (err, data) {
        if (err) {
            res.end(err);
        }
        res.end(data);
    });
});

/**
 * name: mongodb验证接口
 * version: V1
 * return: true/false/error
 */
router.post('/tester/api/V1/testMongo', function (req, res) {
    var statement = req.param('statement');
    testMongo(statement, function (err, data) {
        if (err) {
            res.end(err);
        }
        res.end(data);
    });
});

/**
 * name: API执行接口
 * version: V1
 * return: 执行结果，json串格式待定
 */
router.post('/tester/api/V1/executeApi', function (req, res) {
    var apiurl = req.param('apiurl');
    var method = req.param('method');
    var form = req.param('form');

    if (!apiurl || !method || !form) {
        res.end('error');
    }

    executeApi({
        apiurl: apiurl,
        method: method,
        form: form
    }, function (err, data) {
        if (err) {
            res.end(err);
        }
        res.end(data);
    });
});

module.exports = router;