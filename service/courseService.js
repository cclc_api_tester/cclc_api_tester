var async = require('async');

var Course = global.db.model('course');
var Util = require('../tools/util');


/**
 * 添加新的流程
 */
var addCourse = function (param, callback) {
    Course.saveCourse(param, function (err, data) {
        if (!err) {
            callback(null, {
                status: 1
            });
        } else {
            callback(null, {
                error: err,
                status: 0
            })
        }
    });
};


/**
 * 修改一个现有流程
 */
var modifyCourse = function (param, callback) {
    Course.updateCourseByMenuId(param, function (err, data) {
        if (!err) {
            callback(null, {
                status: 1
            });
        } else {
            callback(null, {
                error: err,
                status: 0
            })
        }
    });
};

/**
 * 查找一个现有流程
 */
var getCourseByMenuId = function (param, callback) {
    Course.getOneByMenuId({ menuId: param.menuId }, function (err, data) {
        callback(err, data);
    });
};

var orderTaskByMenuId = function (param, callback) {
    getCourseByMenuId({ menuId: param.menuId }, function (err, data) {
        if (err) {
            callback(err);
        } else {

            try {
                data.tabList;
            } catch (e) {
                callback({
                    status: 2,
                    error: 'tabList is null!'
                })
                return;
            }

            if (Util.isEmptyObject(data.tabList)) {
                callback({
                    status: 2,
                    error: 'tabList is null!'
                })
                return;
            }

            var tabList = data.tabList;
            var stepIndex = data.stepIndex;
            // -----
            // 处理逻辑待编写
            // -----
            callback(null, {
                tabList: tabList,
                stepIndex: stepIndex
            });
        }
    });
};

module.exports = {
    getCourseByMenuId: getCourseByMenuId,
    addCourse: addCourse,
    modifyCourse: modifyCourse,
    orderTaskByMenuId: orderTaskByMenuId
};