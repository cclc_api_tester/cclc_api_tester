var express = require('express');
var mysql = require('mysql');
var mongodb = require('mongodb');

var Config_mongo = require('../config/setting').mongo;
var Config_sql = require('../config/setting').mysql;
var Util = require('../tools/util');

var sqlPool = mysql.createPool({
    host: Config_sql.host,
    user: Config_sql.user,
    password: Config_sql.password,
    port: Config_sql.port,
    database: Config_sql.database
});

var server = new mongodb.Server(
    Config_mongo.host,
    Config_mongo.port,
    {
        auto_reconnect: true
    }
);
var mongoConn = new mongodb.Db(
    Config_mongo.database,
    server,
    {
        safe: true
    }
);

var testSql = function (param, callback) {
    var statement = param.statement;

    if (Util.isEmptyString(statement)) {
        callback({
            status: 0,
            error: 'sql statement is null'
        });
        return;
    }

    sqlPool.getConnection(function (error, conn) {
        if (!error) {
            conn.query(statement, function (err, result) {
                conn.release();
                if (err) {
                    callback({
                        status: 0,
                        error: err
                    });
                }
                else {
                    callback(null, {
                        status: 1,
                        data: result
                    });
                }
            })
        } else {
            callback({
                status: 0,
                error: error
            });
        }
    });
}

var testMongo = function (param, callback) {
    var statement = param.statement;

    if (Util.isEmptyString(statement)) {
        callback({
            status: 0,
            error: 'mongo statement is null'
        });
        return;
    }

    var reg = [
        /db\.(.*)\.find\((.*)\)/,
        /db\.(.*)\.findOne\((.*)\)/
    ];

    var data = statement.match(reg[0]) || statement.match(reg[1]);;

    if (!data) {
        callback({
            status: 0,
            error: 'mongo syntax error!'
        });
        return;
    }

    data[2] = data[2] || '{}';

    mongoConn.open(function (err, db) {
        if (err) {
            mongoConn.close();
            callback({
                status: 0,
                error: 'error connected to MongoDB!'
            });
            return;
        }
        db.collection(data[1], { safe: true }, function (errcollection, collection) {
            if (errcollection) {
                mongoConn.close();
                callback({
                    status: 0,
                    error: errcollection
                });
                return;
            }
            if (statement.match(reg[0])) {
                var template = `
                    collection.find(${data[2]}).toArray(function(errorfind, cols) {
                        if (!errorfind) {
                            mongoConn.close();
                            callback(null, {
                                status: 1,
                                data: cols
                            });
                        }
                    });
                `;
                eval(template);
            } else {
                var template = `
                    collection.findOne(${data[2]}, function(errorfind, col) {
                        if (!errorfind) {
                            mongoConn.close();
                            callback(null, {
                                status: 1,
                                data: col
                            });
                        }
                    });
                `;
                eval(template);
            }
        });
    });
};

var testStatement = function (param, callback) {
    var statement = param.str;
    var type = param.type;

    if (type == 0) {
        testSql({
            statement: statement
        }, function (err, results) {
            if (err) {
                callback(err);
                return;
            }

            if (Util.isEmptyString(results.data) || results.data.length === 0) {
                // results.status = 2;
                callback('sql result is null!');
                return;
            }

            callback(null, results);
        });
    } else if (type == 1) {
        testMongo({
            statement: statement
        }, function (err, results) {
            if (err) {
                callback(err);
                return;
            }

            if (Util.isEmptyString(results.data) || results.data.length === 0) {
                // results.status = 2;
                callback('mongo result is null!');
                return;
            }

            callback(null, results);
        });
    }
};

module.exports = {
    testSql: testSql,
    testMongo: testMongo,
    testStatement: testStatement
};

