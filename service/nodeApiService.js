﻿var fs = require('fs');
var async = require('async');
var request = require('request');

var excuteDbService = require('./excuteDbService');
var testStatement = excuteDbService.testStatement;

var assertService = require('./assertService');
var assertIt = assertService.assertIt;

var Environments = require('../config/setting').test_enviroment;
var Util = require('../tools/util');

/**
 * 发起NodeAPI请求
 */
var executeNodeApi = function (data_buffer, task, apiLog, callback) {
    
    var url = Environments.nodejsUrl + '/' + task.apiurl;
    url = 'http://' + url.replace(/\/\//g, '\/');

    var options = {
        method: task.method,
        url: url,
        qs: Environments.params,
        headers: Environments.headers,
        form: task.form
    };

    // 发起API请求
    request(options, function (err, res, body) {

        try {
            body = JSON.parse(body);
        } catch (parase_err) {
            callback(parase_err, body);
            return;
        }

        // 记录API请求日志
        apiLog['request'] = options;
        apiLog['result'] = body;

        callback(err, body);
    });
};

/**
 * 按序批量执行NodeApi
 */
var batchNodeApi = function (tasks_obj, callback) {
    // 当前执行任务的id索引号
    var id;
    // 存放tasks对象
    var tasks = tasks_obj.tabList;
    // 记录各个任务的ID号
    var task_ids = tasks_obj.stepIndex;
    // 缓存上一次API接口返回的结果
    var data_buffer = {};
    // 日志记录
    var recode;

    // 初始化日志
    var records = {};

    async.eachSeries(task_ids, function (item, next) {

        // 通过id号获取到未解析过的任务对象
        var raw_task = tasks[item];

        // 记录当前任务的id号
        id = item;

        try {
            // 序列化表单对象
            raw_task.form = JSON.parse(raw_task.form);
        } catch (e) {
            callback(null, {
                id: id,
                status: 0,
                error: '' + e,
                records: records,
                stepIndex: task_ids,
                data: data_buffer
            });
            return;
        }

        // 动态解析
        var parseObj = parseData(raw_task, tasks, records);
        var task = parseObj.jsonData;

        if (parseObj.error) {
            callback(null, {
                id: id,
                status: 0,
                error: '' + parseObj.error,
                records: records,
                stepIndex: task_ids,
                data: data_buffer
            });
            return;
        }

        // 生成单个日志记录对象
        record = records[id] = {};
        record['nodeApi'] = {};
        record['assert'] = [];
        record['db'] = [];

        executeNodeApi(data_buffer, task, record['nodeApi'], function (err, data) {
            if ((!data.errcode) && (err || data.err)) {
                next(err || data.err);
            } else {
                // 缓存本次API的返回结果
                data_buffer = data;

                // 向form中添加请求的返回结果
                for (var o in data_buffer) {
                    raw_task.form[o] = data_buffer[o];
                }

                // 动态解析为断言做准备
                parseObj = parseData(raw_task, tasks, records);
                task = parseObj.jsonData;

                if (parseObj.error) {
                    callback(null, {
                        id: id,
                        status: 0,
                        error: '' + parseObj.error,
                        records: records,
                        stepIndex: task_ids,
                        data: data_buffer
                    });
                    return;
                }

                async.waterfall([
                    function (w_next) {

                        // 验证断言
                        if (Util.isNotEmptyString(task.assert)) {

                            // 清除注释
                            var clAssert = clearAnnotation(task.assert);

                            var assert_arr = clAssert.split(';');
                            var assertLog = {};

                            for (var i = 0; i < assert_arr.length; i++) {
                                var assert = assert_arr[i];

                                if (Util.isNotEmptyString(assert)) {
                                    assertObj = assertIt({ assert: assert });

                                    // 断言日志记录
                                    assertLog = assertObj;
                                    record['assert'].push(assertLog);
                                }
                            }

                            w_next(null);
                        } else {
                            w_next(null);
                        }
                    },
                    function (w_next) {
                        // 验证SQL
                        if (Util.isNotEmptyString(task.mysql)) {

                            // 清除注释
                            var clStr = clearAnnotation(task.mysql);

                            var dbstr_arr = clStr.split(';');
                            var dbLog = {};

                            async.eachSeries(dbstr_arr, function (dbstr, db_next) {
                                if (Util.isNotEmptyString(dbstr)) {
                                    testStatement({
                                        str: dbstr,
                                        type: 0 // type 0 执行sql
                                    }, function (sql_err, sql_data) {
                                        ;
                                        // 数据库日志记录
                                        dbLog = {};
                                        dbLog['type'] = 'SQL';
                                        dbLog['str'] = dbstr;
                                        dbLog['result'] = sql_err || sql_data;
                                        record['db'].push(dbLog);

                                        if (sql_err) {
                                            db_next(sql_err);
                                        } else {
                                            db_next(null);
                                        }
                                    });
                                } else {
                                    db_next(null);
                                }
                            }, function (db_err, db_data) {
                                if (db_err) {
                                    w_next(db_err);
                                } else {
                                    w_next(null);
                                }
                            });

                        } else {
                            w_next(null);
                        }
                    },
                    function (w_next) {
                        // 验证Mongo
                        if (Util.isNotEmptyString(task.mongo)) {

                            // 清除注释
                            var clStr = clearAnnotation(task.mongo);

                            var dbstr_arr = clStr.split(';');
                            var dbLog = {};

                            async.eachSeries(dbstr_arr, function (dbstr, db_next) {
                                if (Util.isNotEmptyString(dbstr)) {
                                    testStatement({
                                        str: dbstr,
                                        type: 1 // type 1 执行mongo
                                    }, function (sql_err, sql_data) {
                                        ;
                                        // 数据库日志记录
                                        dbLog = {};
                                        dbLog['type'] = 'Mongo';
                                        dbLog['str'] = dbstr;
                                        dbLog['result'] = sql_err || sql_data;
                                        record['db'].push(dbLog);

                                        if (sql_err) {
                                            db_next(sql_err);
                                        } else {
                                            db_next(null);
                                        }
                                    });
                                } else {
                                    db_next(null);
                                }
                            }, function (db_err, db_data) {
                                if (db_err) {
                                    w_next(db_err);
                                } else {
                                    w_next(null);
                                }
                            });

                        } else {
                            w_next(null);
                        }
                    }
                ], function (w_err, w_data) {
                    next(w_err);
                });

            }
        });

    }, function (err) {
        if (err) {
            callback(null, {
                id: id,
                status: 0,
                error: '' + err,
                records: records,
                stepIndex: task_ids,
                data: data_buffer
            });
        } else {
            callback(null, {
                id: null, // 执行成功时不返回ID
                status: 1,
                error: null,
                records: records,
                stepIndex: task_ids,
                data: data_buffer
            });
        }
    });
}

// 组织每个folders之下的条目
var getRequests = function (obj, id) {
    for (var i = 0; i < obj['requests'].length; i++) {
        var item = obj['requests'][i];
        var name = item.name;

        if (item['id'] === id) {
            var url = item['url'].replace(/{{[^{]*}}/g, '');

            return {
                'id': id,
                'text': name,
                'attributes': {
                    'url': url,
                    'method': item['method'],
                    'data': item['data']
                }
            }
        }
    }
}

/**
 * 将postman格式JSON数据转换为所需的JSON数据格式
 */
var getNodeApi = function (fileName) {

    var data = fs.readFileSync(fileName, 'utf8');
    var obj = JSON.parse(data);
    var folders = { 'folders': [] };
    var res = {
        id: obj.id,
        text: obj.name,
        children: []
    };

    // 组织folders项
    for (var i = 0; i < obj['folders'].length; i++) {
        var item = obj['folders'][i];
        var id = item.id;
        var name = item.name;
        var order = item.order;
        var children = [];

        for (var j = 0; j < order.length; j++) {
            children.push(getRequests(obj, order[j]));
        }

        res['children'].push({
            'id': id,
            'text': name,
            'children': children
        });
    }
    
    return res;
}

/**
 * 占位符解析函数
 */
var parseData = function (str, tasks, records) {
    var jsonData;

    try {
        str = JSON.stringify(str);

        str = str.replace(/\\n/g, '')
            .replace(/\\t/g, '')
            .replace(/\\b/g, '');

        jsonData = eval('`' + str + '`');
        jsonData = JSON.parse(jsonData);
    } catch (err) {
        console.log(jsonData);
        return {
            error: err,
            jsonData: null
        };
    }

    return {
        error: null,
        jsonData: jsonData
    };
};

/**
 * 注释清除函数
 */
var clearAnnotation = function (str) {
    var reStr;
    reStr = str.replace(/(\/\*.*\*\/)/g, '');
    return reStr;
};

module.exports = {
    getNodeApi: getNodeApi,
    batchNodeApi: batchNodeApi
};