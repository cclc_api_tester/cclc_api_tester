var async = require('async');

var Menu = global.db.model('menu');
var Util = require('../tools/util');

/**
 * 增
 */
var addMenu = function (param, callback) {
    Menu.saveMenu(param, function (err, data) {
        if (!err) {
            callback(null, {
                status: 1
            });
        } else {
            callback(null, {
                error: err,
                status: 0
            })
        }
    });
};


/**
 * 删
 */
var deleteMenu = function (param, callback) {
    Menu.removeMenuById(param, function (err, data) {
        if (!err) {
            callback(null, {
                status: 1
            });
        } else {
            callback(null, {
                error: err,
                status: (err == 'could not has children!') ? 2 : 0
            });
        }
    });
}

/**
 * 改
 */
var modifyMenu = function (param, callback) {
    Menu.updateMenuById(param, function (err, data) {
        if (!err) {
            callback(null, {
                status: 1
            });
        } else {
            callback(null, {
                error: err,
                status: 0
            })
        }
    });
};

/**
 * 查
 */
var getMenu = function (param, callback) {

    // 获取对应父节点下的子节点
    var getChildren = function (parentId, data) {
        var result = [];
        for (var i = 0; i < data.length; i++) {

            var item = data[i];
            if (parentId === item.parentId) {
                result.push(item);
            }
        }
        return result;
    };

    // 递归获取菜单项
    var getMenuById = function (parentId, data) {
        var arr = getChildren(parentId, data);
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            // 如果不是子节点则进行递归
            if (item.attributes.status !== 2) {
                item.children = getMenuById(item.id, data);
            }
        }
        return arr;
    };

    async.waterfall([
        function (next) {

            Menu.getAll(function (err, data) {
                if (!err && Util.isNotEmptyObject(data)) {
                    var arr = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        arr.push({
                            id: item._id.toString(),
                            text: item.text,
                            parentId: item.parentId,
                            iconCls: item.status === 1 ? 'icon-menuManage' : (item.status === 0 ? 'icon-home' : ''),
                            attributes: {
                                status: item.status,
                                des: item.des
                            },
                            children: []
                        });
                    }
                    next(null, getMenuById('0', arr));
                } else {
                    next('error!');
                }
            });

        }
    ], function (err, result) {
        callback(err, result);
    });
};

module.exports = {
    addMenu: addMenu,
    deleteMenu: deleteMenu,
    modifyMenu: modifyMenu,
    getMenu: getMenu
}