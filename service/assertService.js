﻿var Util = require('../tools/util');

/**
 * 断言函数
 * expect(2>1, true, desc);
 */
var assertIt = function (param) {
    var assert = param.assert;

    if (Util.isEmptyString(assert)) {
        return {
            assert: assert,
            error: 'assert statement is null'
        };
    }

    var reg = [
        /expect\((.*),(.*),(.*)\)/
    ];

    var data = assert.match(reg[0]);

    if (!data) {
        return {
            assert: assert,
            error: 'assert syntax error!'
        };
    }

    var actual;
    var expected;
    var desc;

    try {
        actual = eval(data[1].toString());
        expected = eval(data[2].toString());
        desc = data[3];
    } catch (err) {
        return {
            assert: assert,
            error: err
        };
    }

    // 确保第一个参数的解析值是true/false
    if (actual !== true && actual !== false) {
        return {
            assert: assert,
            error: 'first param must be true or false!'
        };
    }

    // 确保第二个参数的解析值是true/false
    if (expected !== true && expected !== false) {
        return {
            assert: assert,
            error: 'second param must be true or false!'
        };
    }

    if (actual === expected) {
        return {
            error: null,
            assert: assert,
            result: 'Pass',
            reason: data[1] + ' is equal ' + data[2],
        };
    } else {
        return {
            error: desc,
            assert: assert,
            result: 'Failure',
            reason: data[1] + ' should equal ' + data[2],
        };
    }


};

module.exports = {
    assertIt: assertIt
};